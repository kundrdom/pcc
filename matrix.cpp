//
// Created by domin on 29.12.2022.
//
#include <iostream>
#include <iomanip>
#include <functional>
#include "matrix.hpp"
#include "matrixHelper.hpp"

// A really tiny number
const double EPS = 1e-9;

// Gaussian elimination
Solution gaussian_elimination(Equations &eq) {
    Solution sol;
    sol.x.resize(eq.equations, 0.0);
    sol.free_variables.reserve(eq.equations);
    sol.kernel.reserve(eq.equations);
    sol.is_unique = true;
    sol.is_consistent = true;

    for (int i = 0; i < eq.equations; i++) {
        int pivot = i;
        double pivot_val = fabs(eq.matrix[i][pivot]);

        // Find the pivot row
        for (int j = i + 1; j < eq.equations; j++) {
            if (fabs(eq.matrix[j][i]) > pivot_val) {
                pivot = j;
                pivot_val = fabs(eq.matrix[j][i]);
            }
        }

        // If no pivot is found, move to the next column
        if (pivot_val < EPS) {
            sol.free_variables.push_back(i);
            continue;
        }

        // Swap the pivot row with the current row
        swap(eq.matrix[i], eq.matrix[pivot]);
        swap(eq.right_side[i], eq.right_side[pivot]);


        // Eliminate the current variable from the other equations
        for (int j = i + 1; j < eq.equations; j++) {
            double factor = eq.matrix[j][i] / eq.matrix[i][i];
            for (int k = i; k < eq.equations; k++) {
                eq.matrix[j][k] -= eq.matrix[i][k] * factor;
                if (fabs(eq.matrix[j][k]) < EPS) eq.matrix[j][k] = 0.0;
            }
            eq.right_side[j] -= eq.right_side[i] * factor;
            if (fabs(eq.right_side[j]) < EPS) eq.right_side[j] = 0.0;
        }
    }

    // Solve the equations
    for (int i = eq.equations - 1; i >= 0; i--) {
        sol.x[i] = eq.right_side[i];
        for (int j = i + 1; j < eq.equations; j++) {
            sol.x[i] -= eq.matrix[i][j] * sol.x[j];
        }
        if (sol.x[i] == 0 && eq.matrix[i][i] != sol.x[i]) {
            sol.is_consistent = false;
        } else if (fabs(eq.matrix[i][i]) > EPS) {
            sol.x[i] /= eq.matrix[i][i];
        } else {
            sol.is_unique = false;
            if (fabs(sol.x[i]) > EPS) {
                sol.is_consistent = false;
            }
        }
    }

    // Solve the kernel
    if (!sol.is_unique) {
        for (int i = eq.equations-1; i > eq.equations-1-sol.free_variables.size(); i--) {
            vector<double> sub_vector(eq.equations, 0.0);
            sub_vector[i] = 1.0;

            for (int j = eq.equations-1-sol.free_variables.size(); j >= 0; j--) {
                for (int k = j + 1; k < eq.equations; k++) {
                    sub_vector[j] -= eq.matrix[j][k] * sub_vector[k] / eq.matrix[j][j];
                }
            }
            sol.kernel.push_back(sub_vector);
            sub_vector.clear();
        }
    }

    return sol;
}

void printMatrix(const Equations& eq) {
    cout << defaultfloat;
    for (int i = 0; i < eq.equations; ++i) {
        cout << "| ";
        for (int j = 0; j < eq.equations; ++j) {
            cout << setw(matrixHelper::getSizeOfTheBiggestValueInTheColumn(eq, j)) << eq.matrix[i][j] << " ";
        }
        cout << "| " << setw(matrixHelper::getBiggestSizeOfRightSide(eq)) << eq.right_side[i] << " |" << endl;
    }
}

void printSolution(Solution solution, const Equations& eq) {
    if (solution.is_consistent) {
        if (solution.is_unique) {
            cout << "Reseni soustavy linearnich rovnic:" << endl;
            for (int i = 0; i < eq.equations; i++) {
                cout << "x" << i + 1 << " = " << fixed << setprecision(2) << solution.x[i] << endl;
            }
        } else {
            cout << "Soustava ma nekonecne mnoho reseni, zde je partikularni reseni:" << endl;
            for (int i = 0; i < eq.equations; i++) {
                if (i < eq.equations) {
                    cout << "x" << i + 1 << " = " << solution.x[i] << endl;
                }
            }
            cout << "a baze linearniho prostoru pridruzene homogenni soustavy:" << endl;
            for (int i = solution.kernel.size()-1; i >= 0; i--) {
                cout << "( ";
                for (double num : solution.kernel[i]) {
                    cout << num << " ";
                }
                cout << ")" << endl;
            }
        }
    } else {
        cout << "Nekonzistentni system - reseni neexistuje" << endl;
    }
}

// Random double from -1000 to 1000 (used for the values of the matrix)
double get_random_double() {
    static mt19937 mt{ random_device{}() };
    static uniform_real_distribution<> dist(-1000, 1000);
    return dist(mt);
}

// Random int from 1 to 1000 (used for the size of the matrix)
int get_random_dimension() {
    static mt19937 mt{ random_device{}() };
    static uniform_real_distribution<> dist(1, 11);
    return dist(mt);
}

Equations create_random_equations() {
    int random = get_random_dimension();
    Equations eq(random);

    random_equations(eq);

    return eq;
}

Equations create_custom_random_equations(int size) {
    Equations eq(size);

    random_equations(eq);

    return eq;
}

void random_equations(Equations &eq) {
    for (int i = 0; i < eq.equations; ++i) {
        for (int j = 0; j < eq.equations; ++j) {
            eq.matrix[i][j] = get_random_double();
        }
        eq.right_side[i] = get_random_double();
    }
}

//void gaussian_elimination_multi_thread(Equations eq, int start, int end, Solution &sol) {
//    // Perform Gaussian elimination on the submatrix
//    // from start to end rows
//    for (int i = start; i < end; i++) {
//        int pivot = i;
//        double pivot_val = fabs(eq.matrix[i][pivot]);
//
//        // Find the pivot row
//        for (int j = i + 1; j < end; j++) {
//            if (fabs(eq.matrix[j][i]) > pivot_val) {
//                pivot = j;
//                pivot_val = fabs(eq.matrix[j][i]);
//            }
//        }
//
//        // If no pivot is found, move to the next column
//        if (pivot_val < EPS) {
//            sol.free_variables.push_back(i);
//            continue;
//        }
//
//        // Swap the pivot row with the current row
//        swap(eq.matrix[i], eq.matrix[pivot]);
//        swap(eq.right_side[i], eq.right_side[pivot]);
//
//        // Eliminate the current variable from the other equations
//        for (int j = i + 1; j < end; j++) {
//            double factor = eq.matrix[j][i] / eq.matrix[i][i];
//            for (int k = i; k < eq.equations; k++) {
//                eq.matrix[j][k] -= eq.matrix[i][k] * factor;
//                if (fabs(eq.matrix[j][k]) < EPS) eq.matrix[j][k] = 0.0;
//            }
//            eq.right_side[j] -= eq.right_side[i] * factor;
//            if (fabs(eq.right_side[j]) < EPS) eq.right_side[j] = 0.0;
//        }
//    }
//}
//
//Solution multi_thread(Equations eq) {
//    Solution sol;
//    sol.x.resize(eq.equations, 0.0);
//    sol.free_variables.reserve(eq.equations);
//    sol.base.reserve(eq.equations);
//    sol.kernel.reserve(eq.equations);
//    sol.is_unique = true;
//    sol.is_consistent = true;
//
//    // Divide the matrix into 4 submatrices
//    int submatrix_size = eq.equations / 4;
//    vector<thread> threads;
//    threads.emplace_back(gaussian_elimination_multi_thread, ref(eq), 0, submatrix_size, ref(sol));
//    threads.emplace_back(gaussian_elimination_multi_thread, ref(eq), submatrix_size, submatrix_size * 2, ref(sol));
//    threads.emplace_back(gaussian_elimination_multi_thread, ref(eq), submatrix_size * 2, submatrix_size * 3, ref(sol));
//    threads.emplace_back(gaussian_elimination_multi_thread, ref(eq), submatrix_size * 3, eq.equations, ref(sol));
//
//    // Wait for all threads to finish
//    for (auto &thread : threads) {
//        thread.join();
//    }
//
//    // Solve the equations
//    for (int i = eq.equations - 1; i >= 0; i--) {
//        sol.x[i] = eq.right_side[i];
//        for (int j = i + 1; j < eq.equations; j++) {
//            sol.x[i] -= eq.matrix[i][j] * sol.x[j];
//        }
//        if (fabs(eq.matrix[i][i]) > EPS) {
//            sol.x[i] /= eq.matrix[i][i];
//        } else {
//            sol.is_unique = false;
//            if (fabs(sol.x[i]) > EPS) {
//                sol.is_consistent = false;
//            }
//        }
//    }
//
//    if (!sol.is_unique) {
//        for (int i = eq.equations-1; i > eq.equations-1-sol.free_variables.size(); i--) {
//            vector<double> sub_vector(eq.equations, 0.0);
//            sub_vector[i] = 1.0;
//
//            for (int j = eq.equations-1-sol.free_variables.size(); j >= 0; j--) {
//                for (int k = j + 1; k < eq.equations; k++) {
//                    sub_vector[j] -= eq.matrix[j][k] * sub_vector[k] / eq.matrix[j][j];
//                }
//            }
//            sol.kernel.push_back(sub_vector);
//            sub_vector.clear();
//        }
//    }
//    return sol;
//}