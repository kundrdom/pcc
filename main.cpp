#include <iostream>
#include <chrono>
#include <cstring>
#include "matrix.hpp"
#include "fileReader.hpp"

using namespace std;

template<typename TimePoint>
chrono::milliseconds to_ms(TimePoint tp) {
    return chrono::duration_cast<chrono::milliseconds>(tp);
}

void printHelp(){
    cout << "\n--------------------------------------------------------------------" << endl;
    cout << "Vyberte z moznosti, napriklad 1: " << endl;
    cout << "1 - random" << endl;
    cout << "2 - vlastni soubor " << endl;
    cout << "3 - vlastni matice z konzole" << endl;
    cout << "4 - ukoncit program" << endl;
}

void print_question(const Equations& eq) {
    char decision;
    cout << "Chcete vytisknout matici po uprave? A/N" << endl;
    cin >> decision;
    if (tolower(decision) == 'a') {
        printMatrix(eq);
    }
}

void readFromTerminal() {
    int equations_number;

    cout << "Zadejte pocet rovnic: " << endl;
    cin >> equations_number;

    Equations eq(equations_number);

    cout << "Zadejte cisla v rovnici jedno po jednom:" << endl;
    for (int i = 0; i < equations_number; i++) {
        cout << i+1 << ". radek" << endl;
        for (int j = 0; j <= equations_number; ++j) {
            double num;
            if (j == equations_number) {
                cout << "Prava strana:";
                cin >> num;
                eq.right_side[i] = num;
            }
            else {
                cout << "x" << j + 1 << ":";
                cin >> num;
                eq.matrix[i][j] = num;
            }
        }
    }

    cout << "Vase zadana soustava linearnich rovnic:" << endl;
    printMatrix(eq);

    auto start = chrono::high_resolution_clock::now();
    Solution solution = gaussian_elimination(eq);
    auto end = chrono::high_resolution_clock::now();

    if (eq.equations < 20) {
        print_question(eq);
    }

    printSolution(solution,eq);

    cout << "Trvalo to: " << to_ms(end - start).count() << " ms.\n";
}

void readFromFile() {
    ifstream inFile;
    string fileName;

    cerr << "Zvoleny soubor musi byt ulozeny ve slozce /files !" << endl;
    cout << "Zadejte nazev textoveho souboru, napriklad 1 :-]" << endl;
    cin >> fileName;
    inFile.open("../files/" + fileName + ".txt");

    auto eq = fileReader::readMatrixFromFile(inFile);

    cout << "Zvolena soustava linearnich rovnic:" << endl;
    printMatrix(eq);

    auto start = chrono::high_resolution_clock::now();
    Solution solution = gaussian_elimination(eq);
    auto end = chrono::high_resolution_clock::now();

    if (eq.equations < 20) {
        print_question(eq);
    }

    printSolution(solution,eq);

    cout << "Trvalo to: " << to_ms(end - start).count() << " ms.\n";
}

void random() {
    cout << "Chcete si vybrat velikost matice sami (1) nebo to nechate na random generatoru (2)?" << endl;
    int decision;
    cin >> decision;

    Equations eq(0);
    switch (decision) {
        case 1:
            cout << "Zadejte kolik chcete vytvorit rovnic O.o" << endl;
            int size;
            cin >> size;
            if (size > 2000) cout << "Tohle chvilku potrva..." << endl;
            eq = create_custom_random_equations(size);
            break;
        case 2:
            eq = create_random_equations();
            break;
        default:
            cout << "Zvolili jste spatne, vypinam program..." << endl;
            exit(1);
    }

    if (eq.equations <= 11) {
        printMatrix(eq);
    }

    auto start = chrono::high_resolution_clock::now();
    Solution solution = gaussian_elimination(eq);
    auto end = chrono::high_resolution_clock::now();

    if (eq.equations <= 11) {
        print_question(eq);
    }

    printSolution(solution,eq);

    cout << "Trvalo to: " << to_ms(end - start).count() << " ms.\n";
}

int main(int args, char* argv[]) {
    cout << "Vitejte v kalkulacce na reseni soustav linearnich rovnic :-)" << endl;
    if (args > 1 && strcmp(argv[1], "--help") == 0) {
        printHelp();
        return 0;
    }

    int input = 0;
    while (input != 4) {
        printHelp();
        cin >> input;
        switch (input) {
            case 1:
                random();
                break;
            case 2:
                readFromFile();
                break;
            case 3:
                readFromTerminal();
                break;
            case 4:
                cout << "Ukoncuji program :-(" << endl;
                exit(0);
            default:
                cout << "Zvolili jste spatne, vypinam program..." << endl;
                exit(0);
        }
    }
    return 0;
}
