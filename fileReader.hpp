//
// Created by domin on 30.12.2022.
//

#ifndef SEMESTRALKA_FILEREADER_HPP
#define SEMESTRALKA_FILEREADER_HPP

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include "matrix.hpp"

namespace fileReader {
    Equations readMatrixFromFile(ifstream &input) {
        if (!input.is_open()) {
            cerr << "Zadny takovy soubor neexistuje!" << endl;
            exit(1);
        }

        int cols = 0;
        int temp = 0;
        int rows = 0;
        string line;
        vector<vector<double>> values;
        vector<double> sub_vector;

        while(getline(input, line)) {
            if (!line.empty()) {
                rows++;
                stringstream ss(line);
                int num = 0;
                do {
                    ss >> num;
                    sub_vector.push_back(num);
                    temp++;
                } while (!ss.eof());
                values.push_back(sub_vector);
                sub_vector.clear();
                if (cols == 0) cols = temp;
                if (temp != cols) {
                    cerr << "V tomto souboru neni konzistentni matice! :-[" << endl;
                    exit(1);
                }
                temp = 0;
            }
        }

        if (rows != cols-1) {
            cerr << "V tomto souboru je singularni matice! :-(" << endl;
            exit(1);
        }

        Equations eq(rows);

        for (int i = 0; i < values.size(); ++i) {
            for (int j = 0; j < values[i].size(); j++) {
                if (j == values[i].size()-1) {
                    eq.right_side[i] = values[i][j];
                } else {
                    eq.matrix[i][j] = values[i][j];
                }
            }
        }
        return eq;
    }
}

#endif //SEMESTRALKA_FILEREADER_HPP
