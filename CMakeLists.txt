cmake_minimum_required(VERSION 3.23)
project(Semestralka)

set(CMAKE_CXX_STANDARD 14)

add_executable(Semestralka main.cpp matrix.hpp matrix.cpp matrixHelper.hpp fileReader.hpp)