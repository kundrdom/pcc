//
// Created by domin on 29.12.2022.
//

#ifndef SEMESTRALKA_MATRIX_HPP
#define SEMESTRALKA_MATRIX_HPP

#include <vector>
#include <ostream>
#include <cmath>

using namespace std;

struct Equations {
    int equations;  // Number of equations
    vector<vector<double>> matrix;  // Matrix A
    vector<double> right_side;  // Right side of the matrix A|b

    explicit Equations(int n) : equations(n), matrix(n, vector<double>(n)), right_side(n) {}
};

struct Solution {
    vector<double> x;  // Solution vector b
    vector<int> free_variables;  // Indices of free variables
    vector<vector<double>> kernel; // Kernel of the equations
    bool is_unique;  // Flag indicating whether the solution is unique
    bool is_consistent;  // Flag indicating whether the system A|b is consistent
};

Solution gaussian_elimination(Equations& eq);
void printMatrix(const Equations& eq);
void printSolution(Solution solution, const Equations& eq);
Equations create_random_equations();
Equations create_custom_random_equations(int size);
void random_equations(Equations &eq);
//void gaussian_elimination_multi_thread(Equations eq, int start, int end, Solution &sol);
//Solution multi_thread(Equations eq);

#endif //SEMESTRALKA_MATRIX_HPP
