//
// Created by domin on 29.12.2022.
//

#ifndef SEMESTRALKA_MATRIXHELPER_HPP
#define SEMESTRALKA_MATRIXHELPER_HPP

#include <random>
#include "matrix.hpp"

namespace matrixHelper {
    int getSizeOfTheBiggestValueInTheColumn(const Equations &eq, int column) {
        stringstream ss;
        ss << eq.matrix[0][column];
        string num = ss.str();
        int max = num.length();
        for (int i = 1; i < eq.equations; ++i) {
            ss.str("");
            ss << eq.matrix[i][column];
            string h = ss.str();
            int temp = h.length();
            if (temp > max) {
                max = temp;
            }
        }
        return max;
    }

    int getBiggestSizeOfRightSide(const Equations& eq) {
        stringstream ss;
        ss << eq.right_side[0];
        int max = ss.str().length();
        for (int i = 1; i < eq.right_side.size(); ++i) {
            ss.str("");
            ss << eq.right_side[i];
            int temp = ss.str().length();
            if (temp > max) {
                max = temp;
            }
        }
        return max;
    }
}

#endif //SEMESTRALKA_MATRIXHELPER_HPP
