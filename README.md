# Řešení soustav lineárních rovnic
Program dostane na vstupu rozšířenou matici. U té zjistí zdali je singulární nebo regulární. Pokud je singulární vyhazuje error message a ukončuje program. Když je matice naopak regulární, pokračuje program k výpočtu výsledku. Může dojít ke 3 možným výstupům: rozšířená matice má právě jedno řešením, rozšířená matice má nekonečně mnoho řešení, v tomto případě se vypíše jedno partikulární řešení s jeho jádrem (řádkové vektory, které tvoří jádro), a nebo program zjistí, že tato rozšířená matice je nekonzistentní. Program tiskne pouze matice, které mají velikost 11x11 (maximální velikost matice, která se vejde na řádek), jinak tiskne jen výsledek soustavy.

## Ovládání programu
Když se program spustí, vyběhne do konzole nabídka možností. Uživatel si vybere zdali chce vygenerovat náhodnou matici, u té si volí jestli náhodné velikosti od 1 do 11 (zadá dvojku) nebo velikosti, kterou si zvolí sám (zadá jedničku). Poté má možnost nahrát matici ze souboru, u této varianty se program zeptá na název souboru, uživatel zadá pouze název souboru ze složky **/files** bez cesty a přípony, takže pro soubor 1.txt se zadává pouze 1. A nakonec je tu možnost zadání vlastní matice z konzole. Ve složce **/files** je připraveno 7 vstupů:
- 1.txt - 3x3 matice s jedním řešením
- 2.txt - 3x3 matice s jedním řešením se vstupem, který má nekonzistetní mezery
- 3.txt - 10x10 matice s jedním řešením
- 4.txt - 3x3 matice, u které program zjistí, že je nekonzistetní
- 5.txt - 5x5 matice s nekonečně mnoho řešeními
- 6.txt - 4x3 singulární matice
- 7.txt - 2x2 matice, u které program zjistí, že je nekonzistetní 

## Meření
Měření proběhlo na 4 jádrovém i5-1135G7 CPU. Pro matici 4000x4000 trval výpočet výsledku 22432 ms.
